namespace Shared

open System
open System.Text.RegularExpressions
open FParsec

type Todo = { Id: Guid; Description: string }

type Item =
    { Item: string
      UnitPrice: decimal
      Quantity: int }

type Invoice =
    { Items: Item list
      Tip: decimal
      Total: decimal }

type PaymentRequest =
    { Invoice: Invoice
      PhoneNumber: string }

type ErrorMessage = string
type TransactionReference = string

type SuccessPayload =
    { Reference: TransactionReference
      RedirectUrl: string option }

type PaymentStatus =
    | Success of SuccessPayload
    | Failure of ErrorMessage

type FulfilledOrder =
    { Reference: string
      ReturnUrl: string }

module Snipcart =
    let shoppingUrl =
        try
            let url = Environment.GetEnvironmentVariable("SHOP_FRONT")
            url
        with
        | _ -> "https://nyalimuzik.shop"

module Invoice =
    let isValid (phoneNumber: string) =
        Regex.IsMatch(phoneNumber, "^(((88|9[89])\d{7}))$")

    let isValidTnm (phoneNumber: string) =
        Regex.IsMatch(phoneNumber, "^(((88)\d{7}))$")


module Todo =
    let isValid (description: string) =
        String.IsNullOrWhiteSpace description |> not

    let create (description: string) =
        { Id = Guid.NewGuid()
          Description = description }

module Route =
    let builder typeName methodName =
        sprintf "/api/%s/%s" typeName methodName

type IInvoiceApi =
    { fetchInvoice: string -> Async<Invoice option>
      makePayment: string -> Invoice -> Async<PaymentStatus> }
