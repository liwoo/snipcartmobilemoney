module App

open Elmish
open Elmish.React
open Elmish.UrlParser
open Elmish.Navigation

#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif



let route: Parser<Index.Route -> Index.Route, Index.Route> =
    oneOf [
        map Index.Route.Domain (s "" <?> stringParam "publicToken")
    ]

Program.mkProgram Index.init Index.update Index.view
|> Program.toNavigable (parsePath route) Index.urlUpdate
#if DEBUG
|> Program.withConsoleTrace
#endif
|> Program.withReactSynchronous "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
