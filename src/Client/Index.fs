module Index

open Elmish
open Elmish.Navigation
open Fable.Core
open Fable.Remoting.Client
open Feliz.Bulma
open Shared
open Strings

type InvoiceState =
    | Fetching
    | Fetched

type PaymentState =
    | Idle
    | Processing
    | Failed
    | Fatal
    | Successful

type Model =
    { Invoice: Invoice option
      InvoiceStatus: InvoiceState
      PhoneNumberInput: string
      PaymentStatus: PaymentState
      RedirectUrl: string option
      HasToken: bool }

type Msg =
    | GetInvoice of Invoice option
    | SubmitPayment
    | ReceivePaymentResponse of PaymentStatus
    | SetInput of string
    | FulfillOrder
    | TryAgain

type Route = Domain of string option

let invoiceApi =
    Remoting.createApi ()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.buildProxy<IInvoiceApi>


let urlUpdate (result: Option<Route>) model =
    match result with
    | Some (Domain query) ->
        match query with
        | Some param -> { model with HasToken = true }, Cmd.OfAsync.perform invoiceApi.fetchInvoice param GetInvoice
        | _ -> { model with HasToken = false }, Cmd.none
    | _ -> { model with HasToken = false }, Cmd.none


let init result : Model * Cmd<Msg> =
    let invoice =
        { Items = []
          Total = (decimal) 0.0
          Tip = (decimal) 0.0 }

    let model =
        { Invoice = Some invoice
          InvoiceStatus = Fetching
          PaymentStatus = Idle
          PhoneNumberInput = ""
          RedirectUrl = None
          HasToken = false }

    let (model, cmd) = urlUpdate result model

    model, cmd

let update (msg: Msg) (model: Model) : Model * Cmd<Msg> =
    match msg with
    | GetInvoice invoice ->
        { model with
              Invoice = invoice
              InvoiceStatus = Fetched },
        Cmd.none
    | SetInput value -> { model with PhoneNumberInput = value }, Cmd.none
    | TryAgain -> { model with PaymentStatus = Idle }, Cmd.none
    | SubmitPayment ->
        match model.Invoice with
        | Some invoice ->
            let phoneNumber = model.PhoneNumberInput

            let cmd =
                Cmd.OfAsync.perform (invoiceApi.makePayment phoneNumber) invoice ReceivePaymentResponse

            { model with
                  PaymentStatus = Processing },
            cmd
        | _ -> model, Cmd.none
    | ReceivePaymentResponse response ->
        match response with
        | Success payload ->
            { model with
                  PaymentStatus = Successful
                  RedirectUrl = payload.RedirectUrl },
            Cmd.none
        | Failure message ->
            match message with
            | "Fatal Error" -> { model with PaymentStatus = Fatal }, Cmd.none
            | _ -> { model with PaymentStatus = Failed }, Cmd.none
    | _ -> model, Cmd.none


open Feliz
open Feliz.Bulma

let navBrand =
    Bulma.navbarBrand.div [
        Bulma.navbarItem.a [
            prop.href Snipcart.shoppingUrl
            navbarItem.isActive
            prop.style [ style.color color.black ]
            prop.children [
                Html.img [
                    prop.className [ "mr-4" ]
                    prop.src shoppingBasketImg
                    prop.alt shoppingBasketAlt
                ]
                Html.text continueShopping
            ]
        ]
    ]

let containerBox (model: Model) (dispatch: Msg -> unit) =
    match model.Invoice with
    | Some invoice ->
        Bulma.container [
            Bulma.title [
                prop.style [ style.color color.gray ]
                text.hasTextCentered
                prop.text completePurchase
            ]
            Bulma.box [
                Bulma.content [
                    Html.ol [
                        for item in invoice.Items do
                            Html.li [
                                Bulma.columns [
                                    Bulma.column [
                                        column.is9DesktopOnly
                                        prop.className [ "pb-0" ]
                                        prop.style [ style.fontSize 14 ]
                                        prop.text ($"%s{item.Item} x%i{item.Quantity}")
                                    ]
                                    Bulma.column [
                                        column.is3DesktopOnly
                                        text.hasTextRightDesktop
                                        prop.text $"MK %.2f{(item.UnitPrice * (decimal) item.Quantity)}"
                                    ]
                                ]
                            ]
                    ]
                    Bulma.columns [
                        columns.isMobile
                        prop.children [

                            Bulma.column [
                                column.is9DesktopOnly
                                prop.style [
                                    style.textIndent 12
                                    style.fontWeight 600
                                ]
                                prop.text total
                            ]
                            Bulma.column [
                                column.is3DesktopOnly
                                prop.style [ style.textIndent 12 ]
                                text.hasTextRight
                                prop.text $"MK %.2f{invoice.Total}"
                            ]
                        ]
                    ]
                ]

                Bulma.field.div [
                    field.isGrouped
                    match model.PaymentStatus with
                    | Idle ->
                        prop.children [
                            Bulma.container [
                                Bulma.field.div [
                                    helpers.isHiddenTablet
                                    text.isItalic
                                    prop.text phoneFormat
                                ]
                                Bulma.control.p [
                                    control.isExpanded
                                    prop.children [
                                        Bulma.input.number [
                                            match Invoice.isValid model.PhoneNumberInput with
                                            | true -> prop.className [ "is-success" ]
                                            | false -> prop.className [ "is-danger" ]
                                            prop.value model.PhoneNumberInput
                                            prop.placeholder placeHolder
                                            prop.onChange (fun x -> SetInput x |> dispatch)
                                        ]
                                        match Invoice.isValid model.PhoneNumberInput with
                                        | false ->
                                            Bulma.block [
                                                color.hasTextDanger
                                                prop.className [ "is-size-7" ]
                                                prop.text invalidPhoneFormat
                                            ]
                                        | true -> Bulma.block []
                                    ]
                                ]
                                Bulma.control.p [
                                    Bulma.button.a [
                                        color.isPrimary
                                        prop.className [ "my-4" ]
                                        prop.disabled (Invoice.isValidTnm model.PhoneNumberInput |> not)
                                        prop.onClick (fun _ -> dispatch SubmitPayment)
                                        prop.text makePayment
                                    ]
                                ]
                            ]
                        ]
                    | Failed ->
                        prop.children [
                            Bulma.box [
                                prop.className [
                                    "has-background-warning"
                                ]
                                prop.children [
                                    Bulma.block [
                                        Html.span $"%s{failedMakingPayment} "
                                        Html.span [
                                            prop.style [
                                                style.textDecoration textDecorationLine.underline
                                            ]
                                            prop.onClick (fun _ -> dispatch TryAgain)
                                            prop.text tryAgain
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    | Fatal ->
                        prop.children [
                            Bulma.box [
                                prop.className [
                                    "has-background-danger"
                                ]
                                prop.children [
                                    Bulma.block [
                                        color.hasTextWhite
                                        prop.text fatalError
                                    ]
                                ]
                            ]
                        ]
                    | Processing ->
                        prop.children [
                            Bulma.container [
                                Bulma.level [
                                    Bulma.levelItem [
                                        Bulma.subtitle [
                                            title.is6
                                            text.hasTextCentered
                                            prop.style [ style.color color.black ]
                                            prop.text processingPayment
                                        ]
                                    ]
                                ]
                                Bulma.progress [
                                    progress.max 50
                                    progress.isSmall
                                    prop.className [ "is-primary" ]
                                ]
                            ]
                        ]
                    | Successful ->
                        prop.children [
                            Bulma.box [
                                prop.className [
                                    "has-background-primary"
                                ]
                                prop.children [
                                    Bulma.block [
                                        Html.span paymentSuccessful
                                    ]
                                    match model.RedirectUrl with
                                    | Some link ->
                                        Bulma.button.a [
                                            color.isInfo
                                            prop.href link
                                            prop.text claimOrder
                                        ]
                                    | None ->
                                        Bulma.button.a [
                                            color.isInfo
                                            prop.href Snipcart.shoppingUrl
                                            prop.text noRedirectLink
                                        ]
                                ]
                            ]
                        ]
                ]
            ]
        ]
    | _ -> Bulma.block []

let displayError (title: string) (subtitle: string option) =
    Bulma.container [
        prop.children [
            Bulma.level [
                Bulma.levelItem [
                    Bulma.icon [
                        icon.isLarge
                        prop.className [
                            "fas fa-exclamation-triangle fa-4x has-text-danger"
                        ]
                    ]
                ]
            ]
            Bulma.level [
                Bulma.levelItem [
                    Bulma.tile [
                        text.hasTextCentered
                        prop.className [ "my-4"; "is-2" ]
                        prop.style [
                            style.color color.black
                            style.width.fitContent
                        ]
                        prop.text title
                    ]
                ]
            ]
            match subtitle with
            | Some message ->
                Bulma.level [
                    Bulma.levelItem [
                        Bulma.subtitle [
                            text.hasTextCentered
                            prop.className [ "my-4" ]
                            prop.style [
                                style.color color.black
                                style.width.fitContent
                            ]
                            prop.text message
                        ]
                    ]
                ]
            | _ -> Bulma.block []
        ]
    ]

let displayFetching =
    Bulma.container [
        prop.children [
            Bulma.level [
                Bulma.levelItem [
                    Bulma.tile [
                        title.is2
                        text.hasTextCentered
                        prop.style [ style.color color.black ]
                        prop.text fetchingInvoice
                    ]
                ]
            ]
            Bulma.progress [
                progress.max 50
                progress.isSmall
                prop.className [ "is-primary" ]
            ]
        ]
    ]



let view (model: Model) (dispatch: Msg -> unit) =
    Bulma.hero [
        hero.isFullHeight
        color.isPrimary
        prop.style [
            style.backgroundColor color.lightGray
        ]
        prop.children [
            Bulma.heroHead [
                Bulma.navbar [
                    Bulma.container [ navBrand ]
                ]
            ]
            Bulma.heroBody [
                Bulma.container [
                    match model.HasToken with
                    | false -> displayError invalidToken None
                    | true ->
                        match model.InvoiceStatus with
                        | Fetching -> displayFetching
                        | Fetched ->
                            Bulma.column [
                                column.is6
                                column.isOffset3
                                prop.children [
                                    match model.Invoice with
                                    | Some invoice -> containerBox model dispatch
                                    | _ -> displayError invoiceFetchError (Some contactStoreOwner)
                                ]
                            ]
                ]
            ]
        ]
    ]
