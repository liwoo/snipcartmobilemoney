module Strings

let continueShopping = "Continue Shopping"
let total = "Total"
let phoneFormat = "Phone Number Format: 888123456"
let placeHolder = "Enter your TNM Number (888123456)"
let makePayment = "Make Payment!"

let shoppingBasketImg = "/shopping-basket.png"
let shoppingBasketAlt = "Shopping Basket"

let processingPayment = "Please authorize the Payment sent to your Mobile Money"

let paymentSuccessful = "🎉 Congratulations! Your Payment was Successful.  Thank you for doing business with us."

let completePurchase = "Checkout with TNM"

let noRedirectLink = "Order Details Sent to your Email"

let invalidPhoneFormat = "Invalid Phone Number Format (No leading 0 please)"

let fatalError = "There was a technical issue on our side. Please contact the Store Owner to fix it."
let claimOrder = "View your Order Details"

let noTokenFound = "No Public Token"

let tryAgain = "You may try again if here."

let failedMakingPayment = "Could not process the payment at this time. Make sure you entered the correct Pin."

let invalidToken = "You are not authorized to View this Page"

let invoiceFetchError = "Could not Fetch Order Details at this time"

let fetchingInvoice = "Fetching Order Details..."

let contactStoreOwner = "Please Contact the Store Owner"