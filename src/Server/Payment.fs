module Server.Payment

open System.Net.Http
open System.Net.Http.Json
open System.Threading.Tasks
open FSharp.Json
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Http
open Saturn.Application
open Shared
open System
open Server.Invoice
open Server.Storage

type PaymentMethod =
    { Id: string
      Name: string
      IconUrl: string
      CheckoutUrl: string }

type PaymentResponse =
    { Successful: bool
      TransactionId: string option
      ErrorMessage: string option
      RemoteReference: string option }

let paymentMethods =
    [ { Id = "MAL_TNM"
        Name = "TNM Mpamba"
        IconUrl = "https://res.cloudinary.com/tiyeni/image/upload/v1630779763/mpamba_logo.png"
        CheckoutUrl = "https://snippaymw.azurewebsites.net" } ]

let FetchPaymentMethods = paymentMethods

type Status =
    | Failed of string
    | Processing
    | Processed of string

type FulfilmentResponse = { ReturnUrl: string }

let UpdatePayment (reference: string) (state: Status) =
    let httpClient = new HttpClient()

    let snipcartSecret =
        Environment.GetEnvironmentVariable("SNIPCART_SECRET")

    httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer %s{snipcartSecret}")

    let url =
        "https://payment.snipcart.com/api/private/custom-payment-gateway/payment"

    let payload =
        match state with
        | Failed message ->
            {| paymentSessionId = reference
               state = "failed"
               error =
                   Some
                       {| code = "failed_to_process"
                          message = message |} |}

        | Processing ->
            {| paymentSessionId = reference
               state = "processing"
               error = None |}

        | Processed ref ->
            {| paymentSessionId = reference
               state = "processed"
               error = None |}

    async {
        let! response =
            httpClient.PostAsJsonAsync(url, payload)
            |> Async.AwaitTask

        response.EnsureSuccessStatusCode |> ignore

        let! content =
            response.Content.ReadAsStringAsync()
            |> Async.AwaitTask

        let config =
            JsonConfig.create (jsonFieldNaming = Json.lowerCamelCase)

        let fulfilmentResponse =
            Json.deserializeEx<FulfilmentResponse> config content

        return Some fulfilmentResponse
    }


let FulfilPayment (paymentResponse: PaymentResponse) =
    match paymentResponse.Successful with
    | false ->
        match paymentResponse.ErrorMessage with
        | Some message ->
            match paymentResponse.RemoteReference with
            | Some ref -> UpdatePayment ref (Failed message)
            | None -> async { return None }
        | None ->
            match paymentResponse.RemoteReference with
            | Some ref -> UpdatePayment ref Processing
            | None -> async { return None }
    | true ->
        match paymentResponse.RemoteReference with
        | Some ref ->
            match paymentResponse.TransactionId with
            | Some id -> UpdatePayment ref (Processed id)
            | None -> async { return None }
        | None -> async { return None }


let GetTokenFromHeader (ctx: HttpContext) =
    let headers =
        [ for pair in ctx.Request.Headers do
              let key = pair.Key.ToLower()
              let value = pair.Value.[0]
              yield key, value ]
        |> Map.ofList

    match Map.tryFind "referer" headers with
    | None -> None
    | Some referrer ->
        let token = referrer.Split("=").[1]
        Some token

let FakeRequestPaymentWithCtx (phoneNumber: string) (invoice: Invoice) (ctx: HttpContext) =
    async {
        let token = GetTokenFromHeader ctx

        match token with
        | None ->
            let response = Failure "Unauthorized"
            return response
        | Some token ->
            try
                let httpClient = new HttpClient()
                let! checkInvoice = FetchInvoiceAsync httpClient token
                printf $"%A{checkInvoice}"

                let id = "some-random-ting"

                let! delay = Task.Delay(30000) |> Async.AwaitTask

                try
                    let redirect = storage.GetOrder id

                    let payload =
                        { Reference = id
                          RedirectUrl = Some redirect.ReturnUrl }

                    let response = Success payload
                    return response
                with
                | _ ->
                    let payload = { Reference = id; RedirectUrl = None }
                    let response = Success payload
                    return response
            with
            | _ ->
                let response = Failure "Unauthorized"
                return response
    }

let RequestPaymentWithCtx (phoneNumber: string) (invoice: Invoice) (ctx: HttpContext) =
    async {
        let httpClient = new HttpClient()

        let gatewayToken =
            Environment.GetEnvironmentVariable("GATEWAY_API_KEY")

        httpClient.DefaultRequestHeaders.Add("ApiKey", gatewayToken)

        let gatewayBaseUrl =
            Environment.GetEnvironmentVariable("GATEWAY_BASE_URL")

        let token = GetTokenFromHeader ctx

        match token with
        | None ->
            let response = Failure "Unauthorized"
            return response
        | Some token ->
            try
                let! checkInvoice = FetchInvoiceAsync httpClient token
                printf $"%A{checkInvoice}"

                match checkInvoice.Invoice.Amount = invoice.Total with
                | false ->
                    let response = Failure "Unauthorized. Potential Hack!"
                    return response
                | true ->
                    let url =
                        $"%s{gatewayBaseUrl}/tnm/blockingpayments"

                    match checkInvoice.Id with
                    | None ->
                        let response = Failure "No Invoice ID found"
                        return response
                    | Some id ->
                        let request =
                            {| msisdn = $"265%s{phoneNumber}"
                               amount = invoice.Total
                               transactionId = id |}

                        try
                            let! paymentResponseBody =
                                httpClient.PostAsJsonAsync(url, request)
                                |> Async.AwaitTask

                            let! content =
                                paymentResponseBody.Content.ReadAsStringAsync()
                                |> Async.AwaitTask

                            let config =
                                JsonConfig.create (jsonFieldNaming = Json.lowerCamelCase)

                            let paymentResponse =
                                Json.deserializeEx<PaymentResponse> config content

                            match paymentResponse.TransactionId with
                            | Some id ->
                                try
                                    let redirect = storage.GetOrder id

                                    let payload =
                                        { Reference = id
                                          RedirectUrl = Some redirect.ReturnUrl }

                                    let response = Success payload
                                    return response
                                with
                                | _ ->
                                    let payload = { Reference = id; RedirectUrl = None }
                                    let response = Success payload
                                    return response
                            | _ ->
                                match paymentResponse.ErrorMessage with
                                | Some message ->
                                    let response = Failure message
                                    return response
                                | _ ->
                                    let response = Failure "Unknown Message"
                                    return response
                        with
                        | _ ->
                            let response = Failure "Fatal Error"
                            return response
            with
            | _ ->
                let response = Failure "Unauthorized"
                return response
    }
