module Server.Storage

open Shared

type Storage() =
    let fulfilledOrders = ResizeArray<_>()

    member __.GetOrder(reference: string) =
        fulfilledOrders
        |> Seq.find (fun order -> order.Reference = reference)

    member __.AddOrder(order: FulfilledOrder) = fulfilledOrders.Add order


let storage = Storage()