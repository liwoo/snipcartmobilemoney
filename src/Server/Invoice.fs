module Server.Invoice

open System
open System.Net.Http
open FSharp.Json

type Address =
    { Name: string
      StreetAndNumber: string
      PostalCode: string
      Country: string
      City: string
      Surname: string option
      Region: string option }

type SnipcartItem =
    { Name: string
      UnitPrice: decimal
      Quantity: int
      Type: string
      DiscountAmount: decimal
      RateOfTaxIncludedInPrice: float
      Amount: decimal }

type SnipcartInvoicePayload =
    { ShippingAddress: Address option
      BillingAddress: Address
      Email: string
      Language: string
      Currency: string
      Amount: decimal
      TargetId: string
      Items: SnipcartItem list }

type SnipcartInvoice =
    { Invoice: SnipcartInvoicePayload
      PublicToken: string option
      Mode: string option
      Id: string option
      PaymentAuthorizationRedirectUrl: string option }

let snipcartBaseurl =
    match String.IsNullOrEmpty(Environment.GetEnvironmentVariable("SNIPCART_BASE")) with
    | true -> "https://payment.snipcart.com/api"
    | _ -> Environment.GetEnvironmentVariable("SNIPCART_BASE")


let ValidateInvoiceAsync (client: HttpClient) (token: string) =
    let url =
        $"%s{snipcartBaseurl}/public/custom-payment-gateway/validate?publicToken=%s{token}"

    async {
        let! response = client.GetAsync(url) |> Async.AwaitTask
        response.EnsureSuccessStatusCode() |> ignore
        return true
    }

let FetchInvoiceAsync (client: HttpClient) (token: string) =
    let url =
        $"%s{snipcartBaseurl}/public/custom-payment-gateway/payment-session?publicToken=%s{token}"

    async {
        let! response = client.GetAsync(url) |> Async.AwaitTask
        response.EnsureSuccessStatusCode() |> ignore

        let! content =
            response.Content.ReadAsStringAsync()
            |> Async.AwaitTask

        let config =
            JsonConfig.create (jsonFieldNaming = Json.lowerCamelCase)

        return Json.deserializeEx<SnipcartInvoice> config content
    }
