module Server.WebServer

open System.Net.Http
open FSharp.Control.Tasks
open FSharp.Json
open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Logging
open Saturn
open Giraffe
open Server.Payment
open Server.Invoice
open Server.Storage
open Shared

let sampleItems =
    [ { Item = "Wazizur White Tee"
        UnitPrice = (decimal) 12000.00
        Quantity = 2 }
      { Item = "To Whom it May Concern Deluxe"
        UnitPrice = (decimal) 5000.00
        Quantity = 1 } ]

let getOrderDetails (token: string) =
    async {
        let httpClient = new HttpClient()

        try
            let! snipcartInvoice = FetchInvoiceAsync httpClient token

            let snipcartItems =
                snipcartInvoice.Invoice.Items
                |> List.map
                    (fun item ->
                        { Item = item.Name
                          UnitPrice = item.UnitPrice
                          Quantity = item.Quantity })

            let invoice =
                { Items = snipcartItems
                  Tip = (decimal) 0
                  Total = (decimal) snipcartInvoice.Invoice.Amount }

            return Some invoice
        with
        | ex ->
            printf $"%s{ex.Message}"
            return None
    }


let invoiceApi (ctx: HttpContext) =
    let RequestPayment (phoneNumber: string) (invoice: Invoice) =
        RequestPaymentWithCtx phoneNumber invoice ctx

    { fetchInvoice = getOrderDetails
      makePayment = RequestPayment }


let webApp =
    Remoting.createApi ()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromContext invoiceApi
    |> Remoting.buildHttpHandler


let fulfilPayment: HttpHandler =
    fun (next: HttpFunc) (ctx: HttpContext) ->
        task {
            let logger = ctx.GetLogger()
            let! body = ctx.ReadBodyFromRequestAsync()

            let headers =
                [ for pair in ctx.Request.Headers do
                      let key = pair.Key.ToLower()
                      let value = pair.Value.[0]
                      yield key, value ]
                |> Map.ofList

            printf $"%A{headers}"

            let config =
                JsonConfig.create (jsonFieldNaming = Json.lowerCamelCase)

            let paymentResponse =
                Json.deserializeEx<PaymentResponse> config body

            logger.LogInformation("{Response}", paymentResponse)

            let! response = FulfilPayment paymentResponse

            match paymentResponse.TransactionId with
            | Some id ->
                match response with
                | Some fulfilled ->
                    let order = { Reference = id; ReturnUrl = fulfilled.ReturnUrl }
                    storage.AddOrder order
                | None -> printf "No Fulfillment Response Found"
            | _ -> printf "No Remote Reference found"

            ctx.SetStatusCode 202
            return! ctx.WriteStringAsync "Received"
        }

let fetchPaymentMethods: HttpHandler =
    fun (next: HttpFunc) (ctx: HttpContext) ->
        task {
            let logger = ctx.GetLogger()
            let! body = ctx.ReadBodyFromRequestAsync()

            let config =
                JsonConfig.create (jsonFieldNaming = Json.lowerCamelCase)

            try
                let invoice =
                    Json.deserializeEx<SnipcartInvoice> config body

                let httpClient = new HttpClient()

                logger.LogInformation("{Invoice}", invoice)

                let token =
                    match invoice.PublicToken with
                    | Some token -> token
                    | _ -> ""

                try
                    let! isValid = ValidateInvoiceAsync httpClient token
                    return! ctx.WriteJsonAsync FetchPaymentMethods
                with
                | _ ->
                    ctx.SetStatusCode 404
                    return! ctx.WriteStringAsync "Could not Validate Token"

            with
            | _ ->
                ctx.SetStatusCode 400
                return! ctx.WriteStringAsync "Bad Input"
        }

let apiRouter =
    router {
        get "" (text "Hello API")
        post "/payment-events" fulfilPayment
        post "/payment-methods" fetchPaymentMethods
    }

let appRouter =
    router {
        forward "/v1" apiRouter
        forward "" webApp
    }

let app =
    application {
        url "http://0.0.0.0:8085"
        use_router appRouter
        memory_cache
        use_static "public"
        use_gzip
    }

run app
